Git
1. pengertian Git
Git adalah salah satu version control sistem yang diciptakan oleh Linus Tolvalds untuk mencatat setiap perubahan file di code.

2. Manfaat Git
- untuk menyimpan seluruh versi source code
- untuk melakukan kolaborasi dalam membangun sistem menjadi lebih mudah
- untuk berkontribusi dalam project open source
- memudahkan dalam tracking perubahan dan memahami cara deploy modern

3. perintah dalam Git
- Git init : digunakan untuk menginisialisasi repository atau folder.
- Git config : untuk melakukan konfigurasi email dan username yang biasanya disamakan dengan akun gitlab atau git hub. terdapat 2 jenis perintah config, yaitu config --global dan config --local. untuk melihat semua konfigurasi yang ada di git bisa menggunakan perintah git config --list.
- Git status : digunakan untuk mengetahui status file, apakah ada perubahan atau tidak.
- Git add : untuk memasukan file ke staging area.
- Git commit : untuk mencatat perubahan yang dilakukan setelah memasukan file ke staging area.
- Git log : untuk melihat riwayat commit.
- Git clone : untuk mengcopy repo dari server ke local atau komputer.
- Git push : untuk mengirim commit ke server.
- Git pull : untuk mengambil commit dari server jika ada perubahan di server yang dibuat oleh branch lain.
- Git remote : api yang digunakan untuk berinteraksi dari client ke server.

Branch
1. Pengertian
Branch merupakan cabangan dari repo atau copyan yang digunakan dalam mengembangkan fitur baru.

2. Manfaat
- parallel development
- digunakan untuk memanajemen pengembangan. misalnya test branch, unstable branch dan stable branch,

3. Perintah dalam Branch
- git branch : untuk melihat semua branch secara local
- git branch --all : melihat semua branch secara public
- git branch nama_branch : untuk membuat branch baru
- git checkout nama_branch : untuk berpindah branch
- git branch -d nama_branch : untuk menghapus branch
4. git merge nama_branch
merupakan perintah yang digunakan untuk menggabungkan commit dari 2 branch. untuk melakukan git merge pastikan semua file sudah di commit.
5. conflict
merupakan kondisi ketika 2 branch melakukan perubahan di file yang sama. saat terdapat conflict kita tidak bisa melakukan commit. untuk menyelesaikan conflict kita harus memilih salah satu perubahan dan lakukan commit terlebih dahulu, setelah itu perubahan lainnya bisa dilakukan dan lakukan commit sebelum perubahan selanjutnya.

Git GUI
- Sourcetree merupakan software berbasis GUI yang digunakan untuk memudahkan dalam mengoprasikan git. dengan adanya soucetree kita tidak perlu lagi mengetikan perintah git pada terminal.

solving conflict pada Sourcetree
untuk menyelesaikan conflict kita perlu mendownload diff merge dan melakukan konfigurasi diff merge pada Sourcetree.

cara konfigurasi diff merge
1. masuk ke menu option pada Sourcetree
2. pilih diff
3. klik external diff tools dan pilih diff merge
4. pada diff command pilih file pada folder install diff merge dengan format sgdm.exe
5. klik merge tools dan pilih diff merge
6. pada merge command pilih file pada folder install diff merge dengan format sgdm.exe
7. dan klik ok


